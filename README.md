# Сбор метрики [Quake 3](https://gitlab.com/frost.dat/quake-3)

## Запуск

Запуск docker-compose производится следующей командой:
```
docker-compose up -d
```

## Сборка логов и метрики в окружении docker-compose

Для реализации данной задачи по сбору метрики используем связку <code>Grafana</code> + <code>Prometheus</code> + 
<code>cAdvisor</code> + <code>node-exporter</code>, где:

* <code>Grafana</code> - инструмент для визуализации (построение графиков, позволяет оценить динамику рассматриваемого процесса/события и т.д.)
* <code>Prometheus</code> - инструмент для сбора различной метрики и ее хранение
* <code>cAdvisor</code> - инструмент для сбор метрик из контейнеров
* <code>node-exporter</code> - инструмент для сбор метрик хостовой машины